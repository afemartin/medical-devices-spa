# Medical Devices SPA

This SPA is built using the Next.js framework to enable SSR and avoid some of the initial configuration tasks (eg. Webpack, Babel, React SSR, routing).

On top of that, this project use ESLint linter, Prettier formatter and Jest test to avoid mistakes and unify coding style on every commit.

Demo: https://afemartin-medical-devices-spa.herokuapp.com

## Installation

Install npm packages (node and npm required)

```
npm install
```

## Usage

Start SPA on development mode (hot reloading + error logs)

```
npm run dev
```

Start SPA on production mode

```
npm run build
npm run start
```

## Technical decisions

### NextJS + ReactJS over plain HTML + JS

Due to my long experience working with ReactJS I found myself more familiar and much more faster developing code.

And after decide to use ReactJS, NextJS feels the easiest choice to start quickly development being able to focus just on build functionality and avoiding some annoying and repetitive initial configuration.

On a real-life complex project I would use ReactJS and probably avoid NextJS at the point when I found myself expending more time tweaking the default behaviour rather if I would have to implement myself that behaviour in a pure ReactJS ecosystem without NextJS on top.

### Semantic UI over plain CSS

Due to the limited time and my limited design skills I decide to rely on a very well design UI framework with many prepared components ready to use.

On a real-life complex project I would create a themed version of Semantic UI customize to fit my specifics needs.

### react-region-select library over custom code

Probably it is not a perfect fit and the look and feel it is not easy to customize but it avoids to reinvent the wheel.

## Wish list

About good coding practices I would like to add:

- Type checking either using Flow or Typescript.
- More test suite to get higher coverage.
- Extract some of the UI pieces inside each pages to an stateless functional components.

About useful features for analyst I would continue working on the following:

- Improve the home page to show more useful information.
- Save progress of the data input during the analysis on background to avoid lost all information if the analyst don't submit the information.
- Allow go from output page to edit again to make extra changes.
- Add status to each device to allow classified and keep track of the progress and control which kind of actions can be done (eg: "pending", "in progress" and "done").
- Improve output visualization with nicer UI and maybe tooltip with extra information (eg: description).
- Take advantage of device component types to allow a more structured user input.

## Development cost

I invested around 10 hours in this coding exercise, I was able to save some time by reusing some code (and experience) from other coding exercises but I have to spend quite some time finding out a good solution for the region selection functionality.

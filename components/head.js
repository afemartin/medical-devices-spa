import { string } from "prop-types";
import NextHead from "next/head";

const defaultTitle = "Medical Devices SPA";
const defaultDescription = "";

const Head = props => (
  <NextHead>
    <title>{props.title || defaultTitle}</title>
    <meta charSet="UTF-8" />
    <meta
      name="description"
      content={props.description || defaultDescription}
    />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" href="/static/favicon.ico" />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.2/semantic.min.css"
      integrity="sha256-Z8Bv3UrxpRsWTfTPIjCojw5CdRNYSPw3TyxoU8WZSrM="
      crossOrigin="anonymous"
    />
  </NextHead>
);

Head.propTypes = {
  title: string,
  description: string
};

export default Head;

import Link from "next/link";
import { Container, Menu } from "semantic-ui-react";

const NavBar = () => (
  <Menu fixed="top" borderless inverted>
    <Container text>
      <Menu.Item>
        <Link href="/">
          <a>Medical Devices SPA</a>
        </Link>
      </Menu.Item>
    </Container>
  </Menu>
);

export default NavBar;

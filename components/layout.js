import { node, string } from "prop-types";
import { Container, Segment } from "semantic-ui-react";

import Head from "./head";
import NavBar from "./navbar";

const Layout = props => (
  <div>
    <Head title={props.title} description={props.description} />
    <NavBar />
    <Container text style={{ marginTop: "80px", marginBottom: "40px" }}>
      {props.children}
    </Container>
  </div>
);

Layout.propTypes = {
  children: node,
  title: string,
  description: string
};

export default Layout;

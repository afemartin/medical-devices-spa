import React, { Component } from "react";
import Router from "next/router";
import RegionSelect from "react-region-select";
import {
  Button,
  Container,
  Divider,
  Form,
  Header,
  Icon,
  Image,
  Label,
  Segment
} from "semantic-ui-react";

import Layout from "../components/layout";

class DevicePage extends Component {
  static async getInitialProps() {
    // TODO: Replace with real API call to fetch the device information based on the id passed in the query string
    const device = {
      id: 1,
      name: "Covidien Force Triad",
      image: "/static/covidien-force-triad.png"
    };
    // TODO: Replace with real API call to fetch the possible component types available
    const componentTypes = [
      {
        id: 1,
        name: "Button"
      },
      {
        id: 2,
        name: "Wheel"
      },
      {
        id: 3,
        name: "Slider"
      },
      {
        id: 4,
        name: "Plug"
      },
      {
        id: 5,
        name: "Monitor"
      }
    ];
    // Format data fetch from the API into data structure needed to render the dropdown
    const componentTypeOptions = componentTypes.map(componentType => ({
      key: componentType.id,
      text: componentType.name,
      value: componentType.id
    }));
    return { device, componentTypeOptions };
  }

  constructor() {
    super();
    this.state = {
      regions: []
    };
    this.imageRef = React.createRef();
  }

  handleChangeComponentField = (componentIndex, componentField, value) => {
    this.setState(prevState => ({
      regions: prevState.regions.map(
        region =>
          componentIndex === region.data.index
            ? { ...region, data: { ...region.data, [componentField]: value } }
            : region
      )
    }));
  };

  handleSelectRegion = regions => {
    this.setState({
      regions
    });
  };

  handleDeleteComponent = index => {
    this.setState(prevState => ({
      regions: prevState.regions.filter(region => region.data.index !== index)
    }));
  };

  handleSubmit = () => {
    const size = {
      width: this.imageRef.current.clientWidth,
      height: this.imageRef.current.clientHeight
    };
    const components = this.state.regions.map(region => ({
      title: region.data.title,
      description: region.data.description,
      type: region.data.type,
      position: {
        x: region.x,
        y: region.y,
        width: region.width,
        height: region.height
      }
    }));
    // TODO: Replace with call to the API to store the device components information
    localStorage.setItem(
      `device-${this.props.device.id}`,
      JSON.stringify({ size, components })
    );
    Router.push({ pathname: "/output", query: { id: this.props.device.id } });
  };

  regionRenderer = region => {
    return [
      <Label circular color="red" style={{ pointerEvents: "none" }}>
        {region.index + 1}
      </Label>
    ];
  };

  render() {
    return (
      <Layout>
        <Header dividing>Device</Header>
        <p>{this.props.device.name}</p>
        <Header dividing>Image</Header>
        <RegionSelect
          regions={this.state.regions}
          onChange={this.handleSelectRegion}
          regionRenderer={this.regionRenderer}
          constraint
        >
          <img ref={this.imageRef} src={this.props.device.image} />
        </RegionSelect>
        {this.state.regions.map((region, index) => (
          <Segment key={index}>
            <Header dividing>Component {index + 1}</Header>
            <Form>
              <Form.Group widths="equal">
                <Form.Input
                  fluid
                  label="Name"
                  onChange={event =>
                    this.handleChangeComponentField(
                      index,
                      "title",
                      event.target.value
                    )
                  }
                />
                <Form.Select
                  fluid
                  label="Type"
                  onChange={(event, { value }) =>
                    this.handleChangeComponentField(index, "type", value)
                  }
                  options={this.props.componentTypeOptions}
                  search
                  selection
                />
              </Form.Group>
              <Form.Group widths="equal">
                <Form.TextArea
                  label="Description"
                  onChange={event =>
                    this.handleChangeComponentField(
                      index,
                      "description",
                      event.target.value
                    )
                  }
                  rows={2}
                />
              </Form.Group>
              <Button
                onClick={() => {
                  this.handleDeleteComponent(region.data.index);
                }}
              >
                Delete
              </Button>
            </Form>
          </Segment>
        ))}
        <Divider hidden />
        <Button
          primary
          fluid
          size="large"
          disabled={!this.state.regions.length}
          onClick={this.handleSubmit}
        >
          Save
        </Button>
      </Layout>
    );
  }
}

export default DevicePage;

import { Component } from "react";
import { Container, Header, Image, Table } from "semantic-ui-react";
import Link from "next/link";

import Layout from "../components/layout";

class HomePage extends Component {
  static async getInitialProps() {
    // TODO: Replace with real API call to fetch the list of devices that need to be analyzed
    const devices = [
      {
        id: 1,
        name: "Covidien Force Triad"
      }
    ];
    return { devices };
  }

  render() {
    return (
      <Layout>
        <Header dividing>Medical devices</Header>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.props.devices.map(device => (
              <Table.Row key={device.id}>
                <Table.Cell>
                  <Link href={`/device?id=${device.id}`}>
                    <a>{device.name}</a>
                  </Link>
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </Layout>
    );
  }
}

export default HomePage;

import { Component } from "react";
import {
  Button,
  Container,
  Divider,
  Form,
  Header,
  Icon,
  Image,
  Label,
  Segment
} from "semantic-ui-react";

import Layout from "../components/layout";

class OutputPage extends Component {
  static async getInitialProps() {
    // TODO: Replace with real API call to fetch the device information based on the id passed in the query string
    const device = {
      id: 1,
      name: "Covidien Force Triad"
    };
    // TODO: Replace with real API call to fetch the device components stored previously
    const { size, components } = JSON.parse(
      localStorage.getItem(`device-${device.id}`)
    );
    return { device, size, components };
  }

  render() {
    return (
      <Layout>
        <Header dividing>Device</Header>
        <p>{this.props.device.name}</p>
        <Header dividing>Output</Header>
        <div
          style={{
            width: this.props.size.width,
            height: this.props.size.height,
            border: "2px solid black",
            position: "relative"
          }}
        >
          {this.props.components.map((component, index) => (
            <div
              key={index}
              style={{
                top: `${component.position.y}%`,
                left: `${component.position.x}%`,
                width: `${component.position.width}%`,
                height: `${component.position.height}%`,
                border: "2px solid black",
                position: "absolute",
                display: "block"
              }}
            >
              {component.title}
            </div>
          ))}
        </div>
      </Layout>
    );
  }
}

export default OutputPage;

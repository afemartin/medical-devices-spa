import React from "react";
import renderer from "react-test-renderer";

import HomePage from "../index.js";

describe("(Page) Home", () => {
  const props = {
    devices: [
      {
        id: 1,
        name: "Covidien Force Triad"
      }
    ]
  };
  it("renders correctly", () => {
    const page = renderer.create(<HomePage {...props} />);
    const tree = page.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

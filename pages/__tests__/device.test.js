import React from "react";
import renderer from "react-test-renderer";

import DevicePage from "../device.js";

jest.mock("react-region-select", () => props => (
  <mocked {...props}>{props.children}</mocked>
));

describe("(Page) Device", () => {
  const props = {
    device: {
      id: 1,
      name: "Covidien Force Triad",
      image: "/static/covidien-force-triad.png"
    },
    componentTypeOptions: [
      {
        key: 1,
        value: 1,
        name: "Button"
      },
      {
        key: 2,
        value: 2,
        name: "Wheel"
      },
      {
        key: 3,
        value: 3,
        name: "Slider"
      },
      {
        key: 4,
        value: 4,
        name: "Plug"
      },
      {
        key: 5,
        value: 5,
        name: "Monitor"
      }
    ]
  };
  it("renders correctly", () => {
    const page = renderer.create(<DevicePage {...props} />);
    const tree = page.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

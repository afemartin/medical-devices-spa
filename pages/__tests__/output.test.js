import React from "react";
import renderer from "react-test-renderer";

import OutputPage from "../output.js";

describe("(Page) Output", () => {
  const props = {
    device: {
      id: 1,
      name: "Covidien Force Triad"
    },
    size: {
      width: 800,
      height: 600
    },
    components: [
      {
        title: "Left Monitor",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet mollis dignissim. Pellentesque at quam odio. Praesent vel suscipit ipsum. Pellentesque interdum mi velit, non maximus est gravida et. Phasellus tempor urna diam, in pretium leo accumsan eget.",
        type: 5,
        position: {
          x: 10,
          y: 10,
          width: 20,
          height: 16
        }
      },
      {
        title: "Right Monitor",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet mollis dignissim. Pellentesque at quam odio. Praesent vel suscipit ipsum. Pellentesque interdum mi velit, non maximus est gravida et. Phasellus tempor urna diam, in pretium leo accumsan eget.",
        type: 5,
        position: {
          x: 70,
          y: 10,
          width: 20,
          height: 16
        }
      }
    ]
  };
  it("renders correctly", () => {
    const page = renderer.create(<OutputPage {...props} />);
    const tree = page.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
